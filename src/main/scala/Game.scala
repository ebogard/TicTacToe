import scala.collection.immutable
import scala.util.Try

/**
  * Created by ebogard on 6/7/17.
  */
sealed abstract class Board[T](val mat: T)
case class Good[T](override val mat: T) extends Board[T](mat)
case class Bad[T](override val mat: T) extends Board[T](mat)
case class Won[T](override val mat: T, winner: Move) extends Board[T](mat)
case class Position(r: Int, c: Int)

sealed trait Move
case object X extends Move
case object O extends Move



case class IllegalMoveException(message: String) extends Exception
case class BoardIsFullException(message: String) extends Exception

class Game {

  type MATRIX = Seq[Seq[Option[Move]]]

  def init(size: Int): Board[MATRIX] = {
    val mat = for (_ <- 0 until size) yield for (_ <- 0 until size) yield None
    Good[MATRIX](mat)
  }

  def init(string: String): Board[MATRIX] = {
    val asMoves = string.map {
      case 'X' => Some(X)
      case 'O' => Some(O)
      case _ => None
    }

    Good[MATRIX](asMoves.grouped(3).toList)
  }

  def findMove(board: Board[MATRIX], depth: Int, game: Game, move: Move): Board[MATRIX] = {
    val candidates = game.possibles(board, move)
    val scoredCandidates = candidates.map(b => (minmax(b, depth, game, other(move)), b))
    move match {
      case X => scoredCandidates.maxBy(_._1)._2
      case O => scoredCandidates.minBy(_._1)._2
    }
  }

  def other(move: Move): Move = if (move.equals(X)) O else X

  def string(board: Board[MATRIX]): String = {
    val inner = board.mat.map(p => ("" /: p)(_ + _.getOrElse("N")))
    ("" /: inner)(_ + _)
  }

  def print(board: Board[MATRIX]): String = {
    val inner = board.mat.map(p => ("" /: p)(_ + _.getOrElse(" ") + "|").dropRight(1))
    ("" /: inner)(_ + _ + "\n")
  }


  def move(board: Board[MATRIX], pos: Position, move: Move) = Try {
    if (board.mat(pos.r)(pos.c).isDefined) throw IllegalMoveException("Attempt to make a duplicate move")
    if (board.isInstanceOf[Won[MATRIX]]) throw IllegalMoveException("Attempt to modify a won board")
    if (board.isInstanceOf[Bad[MATRIX]]) throw IllegalMoveException("Attempt to modify a bad board")
    val next = Good(board.mat.updated(pos.r, board.mat(pos.r).updated(pos.c, Some(move))))
    update(next, move)
  }

  def possibles(board: Board[MATRIX], piece: Move): immutable.IndexedSeq[Board[MATRIX]] = {
    val all = for (r <- board.mat.indices) yield for (c <- board.mat.indices if board.mat(r)(c).isEmpty)
      yield move(board, Position(r, c), piece).get
    all.flatten
  }

  private def minmax(board: Board[MATRIX], depth: Int, game: Game, move: Move): Int = {
    if (depth.equals(0) || !board.isInstanceOf[Good[MATRIX]]) board match {
      case a: Won[MATRIX] => if (a.winner.equals(X)) 100 + depth else -100 - depth
      case _ => 0
    }
    else {
      val possibles = game.possibles(board, move)
      val scores = possibles.map {
        minmax(_, depth - 1, game, other(move))
      }
      move match {
        case X => scores.max
        case O => scores.min
      }
    }
  }

  private def update(board: Board[MATRIX], move: Move) = {
    if (isWon(board)) Won(board.mat, move)
    else if (isBad(board)) Bad(board.mat)
    else board
  }

  private def isWon(board: Board[MATRIX]): Boolean = diagonals(board) || straights(board)

  private def diagonals(board: Board[MATRIX]): Boolean = left(board) || right(board)

  private def left(board: Board[MATRIX]): Boolean = {
    val diagonal = for (i <- board.mat.indices) yield board.mat(i)(i)
    same(diagonal)
  }

  private def right(board: Board[MATRIX]): Boolean = {
    val diagonal = for (i <- board.mat.indices) yield board.mat(i)(board.mat.length - 1 - i)
    same(diagonal)
  }

  private def straights(board: Board[MATRIX]): Boolean = rows(board) || cols(board)

  private def rows(board: Board[MATRIX]): Boolean = {
    val straight = for (i <- board.mat.indices) yield same(board.mat(i))
    straight.contains(true)
  }

  private def cols(board: Board[MATRIX]): Boolean = {
    val swap = for(c <- board.mat.indices) yield for(r <- board.mat.indices) yield board.mat(r)(c)
    rows(Good(swap))
  }

  private def same(seq: Seq[Option[Move]]): Boolean = seq.forall(x => x.isDefined && x.equals(seq.head))

  private def isBad(board: Board[MATRIX]): Boolean = board.mat.count(m => m.count(_.isDefined).equals(board.mat.length)).equals(board.mat.length)
}
