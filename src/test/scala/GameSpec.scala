/**
  * Created by ebogard on 6/7/17.
  */
import org.scalatest._

class GameSpec extends FlatSpec with Matchers {
  type MATRIX = Seq[Seq[Option[Move]]]

  "A move" should "place a piece in the center spot on a board" in {
    val game = new Game()
    val board = game.move(game.init(3), Position(1, 1), X)
    assert(board.isSuccess)
    assert(board.get.isInstanceOf[Good[MATRIX]])
    assert(board.get.mat(1)(1).equals(Some(X)))
  }

  it should "place a piece in the bottom left spot on a board" in {
    val game = new Game()
    val board = game.move(game.init(3), Position(2, 0), O)
    assert(board.isSuccess)
    assert(board.get.isInstanceOf[Good[MATRIX]])
    assert(board.get.mat(2)(0).equals(Some(O)))
  }

  it should "throw PieceAlreadyExistsException if a piece is placed in the same spot twice" in {
    val game = new Game()
    a [IllegalMoveException] should be thrownBy {
      val board = game.move(game.init(3), Position(1, 1), X)
      game.move(board.get, Position(1, 1), O).get
    }
  }

  it should "return a won board for X because they have three in a row on the left diagonal" in {
    val game = new Game()
    val a = game.move(game.init(3), Position(0, 0), X)
    val b = game.move(a.get, Position(1, 1), X)
    val c = game.move(b.get, Position(2, 2), X)
    assert(c.get.isInstanceOf[Won[MATRIX]])
    assert(c.get.asInstanceOf[Won[MATRIX]].winner.equals(X))
  }

  it should "return a won board for O because they have three in a row on the right diagonal" in {
    val game = new Game()
    val a = game.move(game.init(3), Position(0, 2), O)
    val b = game.move(a.get, Position(1, 1), O)
    val c = game.move(b.get, Position(2, 0), O)
    assert(c.get.isInstanceOf[Won[MATRIX]])
    assert(c.get.asInstanceOf[Won[MATRIX]].winner.equals(O))
  }

  it should "return a won board for X because they have three in a row on the middle straight" in {
    val game = new Game()
    val a = game.move(game.init(3), Position(1, 0), X)
    val b = game.move(a.get, Position(1, 1), X)
    val c = game.move(b.get, Position(1, 2), X)
    assert(c.get.isInstanceOf[Won[MATRIX]])
    assert(c.get.asInstanceOf[Won[MATRIX]].winner.equals(X))
  }

  it should "return a won board for O because they have three in a row on the middle up" in {
    val game = new Game()
    val a = game.move(game.init(3), Position(0, 1), O)
    val b = game.move(a.get, Position(1, 1), O)
    val c = game.move(b.get, Position(2, 1), O)
    assert(c.get.isInstanceOf[Won[MATRIX]])
    assert(c.get.asInstanceOf[Won[MATRIX]].winner.equals(O))
  }

  it should "return a good board because no players have won yet" in {
    val game = new Game()
    val a = game.move(game.init(3), Position(0, 1), O)
    val b = game.move(a.get, Position(1, 1), O)
    val c = game.move(b.get, Position(2, 1), X)
    val d = game.move(c.get, Position(2, 2), X)
    assert(d.get.isInstanceOf[Good[MATRIX]])
  }

  it should "return a bad board because the board is full with no winner" in {
    val game = new Game()
    val a1 = game.move(game.init(3), Position(0, 0), X)
    val b1 = game.move(a1.get, Position(1, 0), O)
    val c1 = game.move(b1.get, Position(2, 0), X)
    val a2 = game.move(c1.get, Position(0, 1), O)
    val b2 = game.move(a2.get, Position(1, 1), O)
    val c2 = game.move(b2.get, Position(2, 1), X)
    val a3 = game.move(c2.get, Position(0, 2), X)
    val b3 = game.move(a3.get, Position(1, 2), X)
    val c3 = game.move(b3.get, Position(2, 2), O)
    assert(c3.get.isInstanceOf[Bad[MATRIX]])
  }

  "Possibles" should "return a list of size 1" in {
    val game = new Game
    val a1 = game.move(game.init(3), Position(0, 0), X)
    val b1 = game.move(a1.get, Position(1, 0), O)
    val c1 = game.move(b1.get, Position(2, 0), X)
    val a2 = game.move(c1.get, Position(0, 1), O)
    val b2 = game.move(a2.get, Position(1, 1), O)
    val c2 = game.move(b2.get, Position(2, 1), X)
    val a3 = game.move(c2.get, Position(0, 2), X)
    val b3 = game.move(a3.get, Position(1, 2), X)
    val moves = game.possibles(b3.get, O)
    assert(moves.length.equals(1))
  }

  it should "return a list of size 9" in {
    val game = new Game
    val moves = game.possibles(game.init(3), X)
    assert(moves.length.equals(9))
  }

  it should "return a list of size 8" in {
    val game = new Game
    val a = game.init(3)
    val b = game.move(a, Position(1, 1), X).get
    val moves = game.possibles(b, X)
    assert(moves.length.equals(8))
  }

  "findMove" should "return a board blocking O from winning" in {
    val game = new Game
    val a = game.init(3)
    val b = game.move(a, Position(0,0), O).get
    val c = game.move(b, Position(0,1), X).get
    val d = game.move(c, Position(1,1), O).get
    val move = game.findMove(d, 7, game, X)
    assert(move.mat(2)(2).getOrElse("Not Defined").equals(X))
  }

  it should "return a blocking X from winning" in {
    val game = new Game
    val a = game.init(3)
    val b = game.move(a, Position(0, 1), X).get
    val c = game.move(b, Position(0, 0), O).get
    val d = game.move(c, Position(1, 1), X).get
    val move = game.findMove(d, 7, game, O)
    assert(move.mat(2)(1).getOrElse("Not Defined").equals(O))
  }

  "string" should "return a string of characters representing the board" in {
    val game = new Game
    val a = game.init(3)
    val b = game.move(a, Position(0, 1), X).get
    val c = game.move(b, Position(0, 0), O).get
    val d = game.move(c, Position(1, 1), X).get
    assert(game.string(d).equals("OXNNXNNNN"))
  }

  "init" should "return a board with X in the middle and O on the bottom center" in {
    val game = new Game
    val a = game.init("NNNNXNNON")
    assert(a.mat(1)(1).contains(X))
    assert(a.mat(2)(1).contains(O))
    assert(a.mat(2)(2).isEmpty)
  }


}
